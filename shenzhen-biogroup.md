---
title: Biology in Synthetic Systems
period: 07-15 January 2019
date: 2018-12-17 12:00:00
published: true
layout: page
---

![]({{site.baseurl}}/ShenzhenPresentationDocumentation7.jpg)
*China Research Trip, January 2019, Biology in Synthetic Systems*

Our research into the field of biology as part of synthetic systems has been guided and challenged by the technological ecosystem of Shenzhen. The group has decided to look into six different aspects relating to our main research field.

- Interaction between people, biology and technological systems

- Researching sustainability in one of the most industrial cities in the world

- Value of green spaces - nature as an economic motivator

- Community health awareness - relationship between people and bacteria

- Attitude and cultural value inherited over generations

- Reconnecting with the physical world through materials

These routes resulted in questioning **how craftmanship could foster a symbiosis between technology and biological systems from which the future of city such as Shenzhen could benefit from?**

Following the trip we have come to define some possible ways forward that could contribute to the answer to the question posed, which could lead to a more sustainable and responsible vision of a future as of this field of interest.

- Shift focus away from short term gains towards long term goals focused on sustainability and well-being
- Embed attitudes and values from crafts in manufacturing processes
- Enhance traditional crafts with networked technologies to scale up production in sustainable ways
- Reconnect people with end products and how they are made

<object data="{{site.baseurl}}/resources/ShenzhenPresentation.pdf" type="application/pdf" width="60%" height="720">
  <p>Alternative text - include a link <a href="{{site.baseurl}}/resources/ShenzhenPresentation.pdf">to the PDF!</a></p>
</object>

To find out more about the research trip, the city and our reflections, each of us have created a web page for you to have a look at.

[Julia Danae Bertolaso](https://mdef.gitlab.io/julia.danae/reflections/Shenzhen/)

[Ilja Aleksandar Panić](https://mdef.gitlab.io/ilja.panic/)

[Fífa Jónsdóttir](https://mdef.gitlab.io/fifa.jonsdottir/reflections/China/)

[Barbara Drozdek](https://mdef.gitlab.io/barbara.drozdek/research%20trip/)

[Nicolàs Viollier]()

<iframe src="https://player.vimeo.com/video/313585394" width="740" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
