---
title: 3 | Computer control cutting
period: 6 February 2019
date: 2018-10-14 12:00:00
term: 1
published: true
---

This week we are learning about computer controlled cutting, which includes using laser and vinyl cutters. I used Grasshopper from Rhino6 to create a parametric design that can be easily readjusted following some testing.

**Laser cutter**
It is not the first time I use the laser cutting the Fab Lab. I import my rhino file or DWG file in rhino on the local computer, and then make sure the layer for cutting is the right colour and the layer for rastering is the right colour too. I then right the command print to set up the speed, power and thickness for the correct material. In this case I was using a 3mm cardboard. Once these are set up I can select my file and place it and scale it according to the machine size and material size.
I then adjust the distance between the material surface and the laser tip so that it is focused and won't burn a thicker line, and tape the corners if it is bending a little in some place. Once this is checked I can then turn on the extractor and send my file to be cut and keep an eye on the machine as long as it is running. Once done I make sure to clean the surface for the next person to use.

### Individual assignment
#### Press fit
![]({{site.baseurl}}/image00035.jpeg)
![]({{site.baseurl}}/image00014.jpeg)
I have decided to use Illustrator to draw the press fit model. I have drawn a basic square based landscape that would slide in each other to create a curvy surface.


### Group assignment
We tested different methods for laser cutters and checked their settings and try understand how they work in order to get better results. We tested with 3mm and 5mm cardboard.

#### Living hinge test
I tested the living hinge on 3mm cardboard and different versions I have drawn in Rhino. I wanted to use scrap material from the Fablab and didn't want to use too much material for the testing. The scale at which I printed it was too small for the living hinge to be solid enough but demonstrated the model drawn.

![]({{site.baseurl}}/image00016.jpeg)
Living hinge design worked to make the material flexible but the lines were too close to each other and ended up breaking after bending several times.

![]({{site.baseurl}}/image00013.jpeg)
![]({{site.baseurl}}/image00011.jpeg)
This hinge worked pretty well and was more flexible than I thought it would be. However, once again the line were to close to each other.

![]({{site.baseurl}}/image00010.jpeg)
![]({{site.baseurl}}/image00021.jpeg)
![]({{site.baseurl}}/image00018.jpeg)
I tried to design three types of living hinge and experimented with other kind of living hinge. The lines were too close to each other and I did the mistake to make the border too narrow and therefore it can break to easily when bent.

![]({{site.baseurl}}/image00009.jpeg)
In the end, the tests were quite inspiring for future texture or experimentation with surfaces.

#### Vinyl cutting
The vinyl cutter which is using a adhesive plastic surface which can be used for stickers or screen print. I have used one of my graphic work made in Illustrator with vectors, and used it as a sticker.
![]({{site.baseurl}}/image00034.jpeg)
![]({{site.baseurl}}/image00012.jpeg)
![]({{site.baseurl}}/image00015.jpeg)

**Raster Engraving Test on Acrylic**
Veronica's documentation is [HERE](https://mdef.gitlab.io/veronica.tran/fab-academy/week-03/)

**Press Fit Test on cardboard**
Alexandre's documentation is [HERE](https://mdef.gitlab.io/alexandre.acsensi/fabacademy/w3computercontrolledcutting.html)

**Kerf Test on plywood**
Tom's documentation is [HERE](https://mdef.gitlab.io/thomas.barnes/fabacademy/Computer_controlled_cutting/)
