---
title: 1 | Project management
period: 23-28 January 2019
date: 2018-10-14 12:00:00
term: 1
published: true
---

## Version Control

Gitlab version control records every commit I have made and whether those have passed or failed. I allows me to look back if I have made a mistake or need to repeat something I don't recall how to do it.

Atom is an open source text editor created by GitHub.
Steps used in Atom:
- Save
- Stage all
- Commit message
- Commit
- Push

I am using *Jekyll* site generator.

Bash terminal is used for commands a computing environment on windows10.
Key commands learned when setting up the website:
- *cd* is to direct to a file
- *cd ..* is to go one step back
- *ls* is to open a file
- *bundle exec jekyll serve* is to build the site and make it available on a local server

I prefer to use markdown to write my weekly assignments and use HTML for web structure.

Note: I am using a computer with Windows10.


## Adding a new page on the website

1 Create a markdown file in my *julia.danae* project file
I use markdown as I have little knowledge in in HTML and it is easier for me at the moment to use markdown.


{% highlight ruby %}
  ---
  layout: fabacademy
  title: fabacademy
  `permalink: /fabacademy/`
  ---
{% endhighlight %}



2 Create a HTML file in my *_layouts* folder
For the *Fab Academy* page, I have created a HTML file in *_layouts* in which I have a code for getting a list of links for every week.

{% highlight ruby %}
  ---
  layout: default
  ---

  <div class="container-small">

    <h1 class="text-center">fabacademy</h1>

    <div class="content">
      {{ content }}
    </div>


    <!-- {% assign reflections = site.reflections | where: "published", true | sort: "date", "last" %} -->


    <div class="post-list">
      {% for fabacademy in site.fabacademy %}
      <article>
        <h2 class="post-title"><a href="{{ fabacademy.url | relative_url }}">{{ fabacademy.title }}</a></h2>
        <div class="post-period">{{ fabacademy.period }}</div>
      </article>
      {% endfor %}
    </div>

  </div>

{% endhighlight %}

3. Create a folder for Fab Academy assignments
In this folder I will create weekly a new folder, like I have done for the first, in which I will put my markdown file and my images used in this week's writing.
I take most of my notes, for weekly assignments, on the markdown editor *Typora*. The text you can see here is wat I have written in this markdown file.


4. Add line to *header* HTML file in the *_includes* folder

{% highlight ruby %}
  <li><a href="{{site.baseurl}}/fabacademy" {% if current[1]=='fabacademy' %}class='active' {% endif %}>Fab Academy</a></li>
{% endhighlight %}

5. Add coding in *_config.yml*
Here in this file I added a new custom collection for the *Fab Academy* page to have the same link system as the one for *Reflections* page.

{% highlight ruby %}
  collections:
    fabacademy:
      output: true
      permalink: /:collection/:title/

  defaults:
    - scope:
        path: ""
        type: "fabacademy"
      values:
        layout: post
{% endhighlight %}

## Error messages

I am still trying to figure out the reason why I am unable to make the *Fab Academy* page work.
My first commit for the page set up resulted with an error message, which is the following:

{% highlight ruby %}
  $ bundle exec jekyll build -d public
  jekyll 3.8.4 | Error:  (/builds/MDEF/julia.danae/_config.yml): did not find expected key while parsing a block mapping at line 44 column 5
  ERROR: Job failed: exit code 1
{% endhighlight %}

After several attempt to commit I then tried to rewrite the line of error and somehow the error message is not coming up anymore.

The last commit worked but the page is not uploading. No error message is coming up.
The link to the page is the following: https://mdef.gitlab.io/julia.danae/fabacademy/

Working on it to try make it work...

**Lost CSS problem**
The problem that caused my website to loose all CSS was that when I created the *fabacademy* page I copied the following code from *_config.yml* and kept the defaults written for both pages, which means it overread the *reflection* file.
Here is the corrected code for this issue:

{% highlight ruby %}
  # custom collections
  collections:
    reflections:
      output: true
      permalink: /:collection/:title/
    fabacademy:
      output: true
      permalink: /:collection/:title/

  defaults:
    - scope:
        path: ""
        type: "reflections"
      values:
        layout: post
    - scope:
        path: ""
        type: "fabacademy"
      values:
        layout: post
{% endhighlight %}

**Header issue solved**
I was using in *Typora* a h5 which wasn't defined in my website. To do this I go in my *_sass* folder, *utilities* and open *-base.scss* file where I added the h5 and defined its size in rem.

{% highlight ruby %}
  h5 {
    font-size: 1.5rem;
  }
{% endhighlight %}

**Quote issue solved**
Whenever I would put a quote written in *Typora*, and put it in my website, it would become huge and the line space would be too small so words words would overlap. The predefined *$leading* was not within the *p* (paragraph) frame.

{% highlight ruby %}
  blockquote {
    margin-top: $leading;
    margin-bottom: $leading;
  }
{% endhighlight %}

To put it as part of the paragraph I added below a second blockquote but as part of *p* in which I could define the size. From there I will be able to play an set it up at the right size I want it to be.

{% highlight ruby %}
  blockquote p {
    font-size: 70px;
    line-height: 80px;
  }
{% endhighlight %}

Fab Academy page should now be working.

[Here](https://www.are.na/julia-bertolaso/web-design-2jkxmv7fqda) are some links that have been useful to me for web design. I will keep updating.
