---
title: 0 | Principles and practices
period: 16 January 2019
date: 2018-10-14 12:00:00
term: 1
published: true
---

## Plan a potential project

I would like to use Fab Academy course to create my tools needed for MDEF main project.

- Machine drawing live changes from Grasshopper Rhino
- Organism generating itself through a controlled pattern, creating a visual that can change according to what has been sensed by the microorganisms
- Create an environment that can control the different states of matter of an ecosystem for a specific matter to behave in

Main project for MDEF is to create a material that can behave, transform and adapt to its environment to create a highly haptic experience that aims to represent the feeling triggered in a natural environment or when in contact with living environment.

What do I want to learn?

Tools relevant to achieve project?
Tools that can record change, that can transform, and adapt (parametric design tools)

- Grasshopper
- animation
- printing
- electricity circuit for light
- sensors (touch/pressure, humidity, elasticity, smell, sound, texture)
- biosensors
