---
title: 5 | 3D Scanning and Printing
period: 20 February 2019
date: 2018-10-14 12:00:00
term: 1
published: true
---

![]({{site.baseurl}}/Untitled22c4d Converted.jpeg)

### 3D scanning
![]({{site.baseurl}}/3dscanning4.jpeg)
I scanned a stool as an exercise to understand what the conditions are to use this Skanect software and Kinect camera. I first tried with a climbing shoe but the object was too small for the software to be able to scan properly. I placed the object in a position giving me enough distance from the object and be able to access it 360 degrees for the scanning.
![]({{site.baseurl}}/3dscanning 3.jpeg)
![]({{site.baseurl}}/3dscanning 1.jpeg)
The settings are straight forward and only a few steps long. I first set up the scene as object, then select bounding box (which indicates the distance needed to scan the object, it will appear as a light green if it is the perfect condition to scan), and finally in process stage, I have put the strategy option under watertight so that the filling is completed and makes it one entity (it can also smooth the surface).

### 3D printing
![]({{site.baseurl}}/3dprinting2.jpeg)
I have chosen a model given on Thingiverse. It is a knotted orbit designed with the Earth as a sphere rolling inside it. I chose this one as it is a quite challenging shape that requires a bit more knowledge on certain settings and more importantly because it is a shape that doesn't allow it to be done in the CNC machine.
After downloading the files and uploading it on the IAAC cloud, I opened it in SLIC3R PRUSA Edition software only used for the PRUSA printer. For other 3D printers available in the fablab, such as ULTIMAKER2, we would usually use CURA as the software to prepare the file for printing. These software do equally the same job, they are called slicers.
![]({{site.baseurl}}/3dsliderview_Moment.jpeg)
For this specific design which has 98% of it's structure not touching the plate, I have to set up the right support for it to be printed properly. The angle of every curve will determine how hard it is for the machine to print and therefore how much support it requires. If the overhang angles are more than 35 degree there is no need for support, but in this case these angles are inferior to this degree. I also have set up the support to be everywhere as there are shapes on top of each other but not touching, it will allow support to be built in between those too.
![]({{site.baseurl}}/3dprinting1.jpeg)
While doing the settings I can view the object in 3D tab, preview tab or with 2D top view tab, and use the slider to see at every layer how it will be constructed and printed and to help see some errors or critical points that need a change in settings.

Other settings:
Infill 20% (in any case, 50% should be enough unless you need an object with a lot of strength to handle a lot of pressure or weight)
Parameter (wall) 3
Layer height 0.2mm (I could put 0.1 to make it smoother and have a better finish but it would take mush longer)
Initial layer height 0.3mm
Line width 0.4mm
Solid layers 4
Top layers 4 (more layers means it will look more smooth at the finish)
Bottom layers 4
Support pattern is zig zag with 4mm spacing.

Object dimensions 84 x 90mm
Printing time 8 hours 24 minutes

![]({{site.baseurl}}/.jpeg)
Once all the settings have been checked or changed, I can export the G-Code onto the SD Card for the PRUSA printer. Then it is pretty easy to send the work to start. I haven't changed the colour type of PLA, so it will be black. PLA is starch and sugar based, which makes it a biodegradable material.

### Next
#### 3D print nature-inspired textures
What if I can represent different state of an element? (ex. smoke printed 3D)
3D scan organic textures to 3D print it and challenge the haptic experience between the real and artificial. I haven't been able to do this yet as the kinect only scans object of a certain size and capturing the texture would have to look at a much shorter distance.
[3D scanning micro objects](http://calculatedimages.blogspot.com/2013/07/micro-3d-scanning-1-focal-depth.html)

#### 3D print using bio materials
I would like to use this week's assignments to start experimenting with the use of biomaterials for 3d printing. I was thinking maybe to 3D print an object using a bacteria medium as a material to print and later progressively grow a bacterial dye on this structure. I will have to discuss with the staff if this can be an option. It will depend on the fluidity and smoothness of the printing material.

[3D printing of bacteria into functional complex materials](http://advances.sciencemag.org/content/3/12/eaao6804)
[3D Bacterial Printing](https://pubs.acs.org/doi/pdf/10.1021/acssynbio.6b00395)
[Bacterial Culture Media Recipes](http://www.thelabrat.com/protocols/bacculture.shtml)

----------
[Knotted orbit](https://www.thingiverse.com/thing:512210)
[RCS Loopie](https://www.thingiverse.com/thing:272580)
[Chain mail trimesh](https://www.thingiverse.com/thing:2611564)
