---
title: 10 |
period: 20 March 2019
date: 2018-10-14 12:00:00
term: 1
published: true
---

**INPUT class**

Why do we need sensors?
Visualise changes happening in our environment to sense our environment (but not only).

Sensors were designed because they were giving outputs which can contribute to closing loop systems. However not every technology has a closed loop system. Example of the 3D printers (...)
Sensors have moved from being scientific to things we carry at all time on us, such as our mobile devices (GPS, compass, ...) Today sensors can, not only to see, but achieve something to move, to change, to control...
Sensors in smartphones are using multiples sensors for single function all together to give as much accuracy as possible.

Three things to take into consideration when designing using sensors:
- Sensors are not only used to measure environments (such as pollution issues) but can be equally used within machines to make it functional.
- Data fusion as a way to increase accuracy of results and functionality.
- Might remove unnecessary sensors when you have enough data for possible changes to be predictable.

How to choose / wire / code for a sensor?
Sensors could be selected as components (+affordable, -coding) but also as devices (+accessibility, -price).
Sensors interfaces (...)
Capacitive sensing (...)

Learning how to make a sensor with basic knowledge and elements form off shelves at home, can contribute to building a resilience to changes.

[Adafruit](https://www.adafruit.com/) is an online platform providing basic knowledge to create at home designs (for instance sensors) using the appropriate components.

Breakout board (...)

Select an input for this week assignment
Light
Detect light from the environment, the sun position change...
http://academy.cba.mit.edu/classes/input_devices/index.html
