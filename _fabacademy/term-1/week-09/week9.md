---
title: 9 | Moulding and Casting
period: 20 March 2019
date: 2018-10-14 12:00:00
term: 1
published: true
---

I first imagined to recycle the copper tape I peeled of my board by melting it and then combine it to another metal to make a ring with a tiny part covered with copper. However I have asked around about it and the copper tape used for the PCB boards is not pure enough for a good quality result. It is better to try to find a good pieces of metal to melt and work with.
Now if I want to stick to the idea of working with metals I need to find the right moulding material for it. One reason for not using other materials such as silicon is because I do not want to have more plastic and un-useful things than I already have in my home.

If I can not work with metals I will be making a stamp that will be used to make my "business" card. I would like to stamp any surfaces using ink to generate my professional information whenever needed but in a professional way rather than hand written or costly when printed. I am not sure if it will look good or would work but might want to use this opportunity to try.
