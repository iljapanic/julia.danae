---
title: Term 3
period: 29 June 2019
date: 2018-12-17 12:00:00
term: 1
published: true
---

<span class="text-small">*Latent Communication development*</span>

### Project time management

<iframe width="60%" height="720" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vSJis78NrQKo0ecfdg09da0yiacGYTb0z097ZttJFaqHMyAjp1JSqfoYGM7lZaXzC3BvwbxR9mknBXI/pubhtml?widget=true&amp;headers=false">
</iframe>

### Planning system
This plan is divided in different categories with different speed for completing. The VISION is essential to reinforce the structure of the project. It aims to clarify what I imagine as the outcome being for the end of this master and what I would want this outcome to evolve into past the master. To do's NOW is a category that will keep being updated weekly, I have now shown the first few days of to dos. It is made of quick or small actions that once done, should inform what will be the next steps short steps. They ensure I keep making to reach my goal. Answers to DEFINE is another category that consists more of researching deeper certain key points that will ensure I am going in the right direction. These should decrease through the weeks and the making should increase. It is also about answering questions, then making decision that will generate smaller questions to make another decision and so on. METHODS for process is ways I could start making, some will have to be done at the same time and converge at some point. Others might be put on the side if found irrelevant as I go on in the days.

### Goal

*Latent Communication* envision an alternative process of photography that reveals the materiality of components used today in digital devices and cameras.

This new vision of photography goes further than ocularcentrism and develops a focus on the touch and physical interactions around the instrument that is mediating the process of capturing moments (physically - lenses and digitally - screens).

This project aims to capture and reveal a new nature of interaction between a digital consumer and its environment, by looking at the materiality of the tool that is enabling this process.

The component that is responsible for the scale, the focus, the format and the aperture we use today to communicate those images is defined by the choice of lens. Playing with variables, such as light, scale, position and movement can affect the result the lens creates and the way one decides to manipulate such tool. This project will result in a new instrument of photogrpahy and a series of experimentation resulting in the use of it. A hybrid process will have been tested, moving between making the physical and making the digital. The type of photography that will be defined along the way of experimentation, will be used as the medium to represent the result of this process. The outcome will allow people to try for themselves the process of bouncing between digital and physical in the process of making a photograph, and observe how the results obtained inform the way they can use the instrument. It would open up the possibilities to create variations of the same moment, influenced by one's interaction with such artifact and therfore alter desires for the future.

#### Intervention proposal for term 3
