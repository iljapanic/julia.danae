---
title: 7 | The Way Things Work
period: 12-18 November 2018
date: 2018-11-18 12:00:00
term: 1
published: true
---

<span class="text-small">*Introduced by Guillem Camprodon, Victor Barberan, Oscar Gonzalez.*</span>

![]({{site.baseurl}}/DSC_0694.jpg)

This week was an introduction to physical computing. It was a reminder of some of the things I had learned during the pre-course such as the basics of Arduino. We were also given a brief history of the digital age, electronics, which helped me understand why things are connected and used in the way they are today. But most importantly, this introduction helped me understand what is the system in which information is created and shared but also how to create a network. It is through the hacking of everyday objects, such as a printer, that I discovered the physical objects which enables this virtual system.


## Brief history of electronics, computers and the digital age

### Internet and networks

I want to look at the meaning of internet in the last half-decade of *globalisation*, and understand what kind of meaning and role it takes in *global village* and in the more recent idea of *Smart City*.

Globalisation can be defined as ["the process by which the world is becoming increasingly interconnected as a result of massively increased trade and cultural exchange."](https://www.bbc.com/bitesize/guides/zxpn2p3/revision/1). Internet has an essential role in that process, allowing better and faster exchange of information resulting in a global communication network. Some of the consequences of this global network, directly seen by the society, were expressed in Obama's speech in 2009 "A New Beginning":
*"I know that for many, the face of globalization is contradictory. The Internet and television can bring knowledge and information, but also offensive sexuality and mindless violence. Trade can bring new wealth and opportunities, but also huge disruptions and changing communities. In all nations – including my own – this change can bring fear. Fear that because of modernity we will lose of control over our economic choices, our politics, and most importantly our identities – those things we most cherish about our communities, our families, our traditions, and our faith."*

*"The new electronic independence re-creates the world in the image of a global village."* (Marshall McLuhan)

The *Internet* is a network of devices linked through global telecommunications. Such technologies are an extension of the human body, and are becoming - if not already - integral parts of our lives. The term IoT (Internet of Things) is the connection between computing devices that are embedded in everyday objects, used via the internet to have a continuous exchange of data.
McLuhan talks about the concept of Global Village to highlight his observation that an electronic nervous system (Global Media Network) was rapidly integrating the planet. Meaning that circumstances happening in one area of the world could be experienced from other places in real-time. This type of experience happens in the same way in small villages.

Before that, Nikolas Tesla described what we could now understand as a Global Village: *"When wireless is perfectly applied the whole earth will be converted into a huge brain, which in fact it is, all things being particles of a real and rhythmic whole. We shall be able to communicate with one another instantly, irrespective of distance. Not only this, but through television and telephony we shall see and hear one another as perfectly as though we were face to face, despite intervening distances of thousands of miles; and the instruments through which we shall be able to do this will be amazingly simple compared with our present telephone. A man will be able to carry one in his vest pocket."*(Nikolas Tesla, Interview with Colliers magazine, 1926)

In 1926,Tesla shared a prediction about the future of what is internet today: *”When wireless is perfectly applied the whole earth will be converted into a huge brain, which in fact it is, all things being particles of a real and rhythmic whole. We shall be able to communicate with one another instantly, irrespective of distance.”*


**Accessibility**

Although globalisation is probably helping to create more wealth in developing countries, it is creating a greater gap between the world's poorest countries and the world's richest.
Population without internet access ((Map) African continent, in majority very low or none existent.)
Centralized, decentralized, distributed networks - Make information more reliable with distributed method, consensus between all those machines.
We have looked at the difference in the use of Internet taking the example of Myanmar, which has a very low amount of internet users (less than three users per 100 people) in comparison with Indonesia which has a high amount of internet users (Quartz data).

Standardization makes things cheaper and faster to produce, however it is not always easy to dismantle and recycle. We have tried to open up and analyse the pieces of a printer, and tried to see if we could hack it by using those to build new devices.

**Information communication**

*”Information Theory”* - information Entropy
https://www.khanacademy.org/computing/computer-science/informationtheory/moderninfotheory/v/information-entropy
How to share an idea? No matter how, they will all carry the same amount of *bit*.

Communication using signals (such as birds and bees through vibrations)
Why encode information and send it? To measure information? what is the *”information scale”*.

*”Information packaging”* (telephone - physically connecting) - package switching


Networks and **RFC** (Request For Comments)
Time sharing - network created
Internet is simply a way for multiple networks to communicate together.
*”Neutral exchange point”* - data centre where connecting all networks together, using cables.
A mesh of connections.


**Tim Berners-Lee** is an engineer and computer scientist, known for being the inventor of “*World Wide Web* (WWW), the first web browser and editor in 1998. At the time it was written, it was the sole web browser in existence, as well as the first WYSIWYG HTML editor. The source code was released into the public domain on April 30, 1993.
It can be defined now as: the way you request files, the web server, the browser, a language (html).

![]({{site.baseurl}}/LRG_DSC08577.jpg)
*Deconstructing a printer*

## Objects as interfaces

Digital electronics can be like *black box* in the way that we don’t really know what is happening in it and how things are happening. The more you look into the abstraction of digital electronics, the more complex it becomes to understand.

What are sensors and actuators?
A sensor is a module that detects change in environment - input device.
An actuator requires a source of energy and a control signal in order to be a motor responsible for controlling and moving a mechanism or a system - output device.
Those two components are necessary to create a circuit, also called an electric system, and they can be either digital or analogue.

Design interfaces are used to communicate with things through connection. For instance the way we code machine to the internet is designing those as interfaces.

We tend to forget that technology are embedded in our lives. We start wearing more and more of those, creating a strong reliance on those. Does it abstract us from reality?
Is it creating an illusion in which we feel instantly and highly connected to reality?


## Building a network in the classroom

Choose a gear with which to exchange information and create a network in an analogue way. (paper, write binary, point)

TCP/IP Model vs. OSI Model
http://fiberbit.com.tw/tcpip-model-vs-osi-model/
Find examples for applications...


Scan network with your computer with [Wireshark](https://www.wireshark.org/)

Use Raspberry Pi as router/ VPN or web server/ wifi extender… Uses Linux - an open source operating system.

Why open-source? 1983 MIT guy did programming around computers and printers. At that time you get a computer and write your drive to make it work on your computer. They would give that code writing to others for others to run that printer on their own computers too.
Today, the top 500 computers in the world run on Linux (Ubuntu a open source Linux distribution).
At that time no copyright, not a business… It was a breaking point when companies realised they could sell those and have control and property around those.

A *”Free software”* is different from an open source software and has different [types of freedoms](https://www.gnu.org/philosophy/free-sw.en.html#f1).
Richard Stallman - free software movement activist.

Set pi
Set network
Read


## MQTT

(Message Queuing Telemetry Transport) used to communicate between devices and works on top of TCP/IP protocol and was developed by Andy Stanford-Clark (IBM).

A function as a package of instructions into one name.

<span class="text-small">
Pi 05 > 192.168.2.151
Pi 03 > 192.168.2.227
Screen 192.168.2.219

Wifi name: Sensors Network
	Passcode: sensors4ever

% ping 192.168.2.219  > checking if it is alive by checking if receiving a response (keeps running if working)
% ping 192.168.2.151
% ping 192.168.2.227

 % ping google.com

% traceroute 192.168.2.151 > trace where package is stopping. If no routers in between it will be a direct link.
% traceroute google.com > allows to find google data center IP.

% whois google.com > will give me all information about it’s ID such as address, phone number,...

% sudo
% mosquitto_pub > send message to the network through terminal - “chat room”.
% mosquitto_ </span>

First, we've installed ESP8266 in Arduino ide adding an address.
Test if the NodeMCU 1.0 board is responding by going into: File > Examples > ESP8266 > Blink.

IP address needs to be shared for the network to know you are there however, keep MAC (media access control) address as private as possible cause it will expose much more information to the public if shared.
IP is used to define a device connected to a network.
Protocol for machine - machine communication: Need to agree on how to get the messages but do not need to remember all addresses.


![]({{site.baseurl}}/PB133288.jpg)
*Arduino set up*

## Project I,O,I…

Select input or output from a technological perspective. Those were created without the need to find something to solve or have a definite purpose, which is something I found challenging. I have always approached design by trying to solve a problem and create a meaningful and purposeful design.

Looking at the physical response to joy through sound.
Use movement to sense joy within a defined space.
Laughing sensor.

How -
Digital microphone measuring frequency (high and low pitch) - complex.
Raspberry pi + digital/analogue microphone.
Pitch of the sound will define how happy.

Data visualisation -
Numbers, letters, shapes, colours, rhythm/flash.

Tools -
	Microphone to record sound

Output -
	Wave of joy using light, colour, movement.
	Score of joy - machine leanring code to detect joy translated into a score.
	Training data set
	Addressable leds used to create?
	Drawing machine

Process -
Draw the coding steps to make the coding more clear in Arduino.
Connect to wifi Sensor Network through ESP8266.
Connect 3 servo motors to board to spin 90 degrees.
Cut leds bands to right length.
Write code to connect leds with motors.
Decide colours of light, intensity and time.

Next -
Decide which light will light up depending of the degree of rotation of motors.




-------------

## References

Alternative search engines:
[Duckduckgo (2008)](https://duckduckgo.com/?t=hp) by Gabriel Weinberg
[Ecosia (2009)](https://www.ecosia.org/?c=en) by Christian Kroll

http://frederickvandenbosch.be/?p=267

[*”Why do we assume that online publishing is greener than print and paper?”* by Barney Cox (Published in 2008)](http://eyemagazine.com/feature/article/foot-prints)

Nikolas Tesla
https://teslauniverse.com/nikola-tesla/timeline/1890-tesla-discovers-wireless-power-tranmission
https://paleofuture.gizmodo.com/nikola-teslas-incredible-predictions-for-our-connected-1661107313

[Cluster at MIRA Festival](https://vimeo.com/206077812)

[Detecting human laughter](https://www.semanticscholar.org/paper/Audiovisual-Detection-of-Laughter-in-Human-Machine-Petridis-Leveque/24e3b89a2079d026de22b3d62b199109137d77c3)

https://academo.org/demos/virtual-oscilloscope/

http://frederickvandenbosch.be/?p=267

[*”Understanding Media: The Extensions of Man “*](https://www.youtube.com/watch?v=OpexbEcsN2A) (Marshall McLuhan, 1964)

*”The Medium is the Message: An Inventory of Effects”* (Marshall McLuhan, 1967)

https://www.electronics-tutorials.ws/io/io_1.html

https://hackmd.io/ys7lcCDJSeq69meSeGnIXg

https://www.wired.co.uk/article/internet-of-things-what-is-explained-iot

Bill Gate's Smart City project in Arizona.
https://www.dezeen.com/2017/11/13/bill-gates-plans-smart-city-arizona-desert-belmont-partners/
https://www.smartcitiesdive.com/news/bill-gates-proposed-smart-city-could-also-be-a-water-constrained-one/527744/

*”Understanding Media: The Extensions of Man “* (1964)
https://www.youtube.com/watch?v=OpexbEcsN2A

*”The Medium is the Message: An Inventory of Effects”* (1967)

[Macintosh (1984) by Apple](https://www.youtube.com/watch?v=tb_EXSIfHjA)
