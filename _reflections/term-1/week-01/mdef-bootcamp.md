---
title: 1 | MDEF Bootcamp
period: 1-7 October 2018
date: 2018-10-07 12:00:00
term: 1
published: true
---
<span class="text-small">*Introduced by Thomas Diez and Oscar Tomico*</span>

![]({{site.baseurl}}/PA042628.jpg)
*Inside the Brain, INDISSOLUBLE The Exhibition Factory, Barcelona, October 2018*


I would like to explore how the use of organisms within the process of biomimicry design could act as a solution to the global environmental problems caused by today’s mass-consumption. And further question the role of technologies and society’s infrastructures.

### Time

Recent work

In the past year, I joined a Design Research Studio aiming to design and research uncertainties, looking at how digital consumerism transforms the design of our city and our experience today and in the future. It has allowed me to create ‘The Flow of Convergence’ , a short film speculating on a potential failure of our digital model and how it could provide a brief moment of awareness of the dependence on the underlying model of technology, which is constructing a reality overloaded with digital information. This project has allowed me to explore collaboratively a new medium with the ultimate aim to share this awareness.

Past

The Industrial revolution of the nineteenth century stimulated the demand for goods in developed areas of the world. The goal, as advertised to consumers, was to facilitate the way of living, but undeniably also to make profit through business. The transition from consumption patterns based on needs to consumption patterns based on desires, seen throughout the 20th century in the West, has resulted in contemporary lifestyles defined by the consumption of material goods.

Present

This evolution has continued and today’s technological advancements have not only allowed greater efficiency in the production, distribution and consumption, but have also created a complex network allowing our economic system to grow faster and bigger than ever before. We have now entered an age of digitalisation, where all data is used to see the predictions of our environment, our performance and behaviour. Data is becoming reality. Our modern social values are greatly defined by the technological revolution defining our landscape. These fast changes have made political ideas obsolete along with its static spa<al construct of the urban environment.

Future

How do we redesign our institutions and our norms? How does architecture fit in this future and could it be built with new means? Could we reinstate nature and its value in the urban environment to allow a coexistence between
different systems, mediated by design? Could we change from a man-made production pace to a natural rhythm, keeping in mind the future we are currently building for ourselves.

In 3 years

I see myself moving between the urban and the natural environment, travelling to keep learning and testing ideas in various contexts. I would want to work with a small team with different backgrounds, create a multicultural environment sharing different skills, knowledge and experiences. During this first week at IAAC, the many discussions and discoveries has made me realise the infinite amount of possibilities there is to design a future. In order to do that I will need to find our who I want to become as a designer but also as an a person.

![]({{site.baseurl}}/PA042613.jpg)
*The Office, INDISSOLUBLE The Exhibition Factory, Barcelona, October 2018*

I’ve also come to believe it might be more impactful and challenging, to design a method, like a language, as a project rather than a product, as it has the capacity to grow, adapt and be applied in different locations. This past week we’ve been given many information and tools to start reflecting on the ways to start our projects, but before I can see the full utility of those, I feel the need to clarify what is the project I want to create.
I am now looking for fruition through research, analysis and prototyping for the creation of tangible outcomes; sharing an awareness and create a change through the making.

![]({{site.baseurl}}/PA042624.jpg)
*Brain Joints, INDISSOLUBLE The Exhibition Factory, Barcelona, October 2018*

What I've learned

- Make use of the large amount of resources available around the area.
- Change will happen through small scale interventions.
- Ideas need to be put in context and tested.
- Sharing of knowledge and skills between people is key.
- Look behind the preconceived ideas.

A haptic experience guided by the touch of materials and their appearance. IAAC is acting as a proxy to the neighbourhood of Poblenou, creating a stronger network between people who want to create new projects for the area.  

![]({{site.baseurl}}/PA042600.jpg)
*Reflections, Barcelona, October 2018*

### Paradigms

Is it bad to want something that is not needed? How do we see the notion of waste now as it might not be the same in a diﬀerent system? Human - Machine - Nature - Machine conversation?  How to converse with nature through machine, for human to read and understand? Use machine as a interface? A language, a translation, a system used to communicate between two very diﬀerent systems? If I want to reach the aim of making an actual positive impact. I need to scale down and actually test the idea in a speciﬁc context and use MDEF multi-scalar design intervention approach.

![]({{site.baseurl}}/PA042641.jpg)
*Tools to Create, Poblenou , Barcelona, October 2018*
