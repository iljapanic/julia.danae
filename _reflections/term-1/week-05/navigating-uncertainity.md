---
title: 5 | Navigating the Uncertainty
period: 29 October - 4 November 2018
date: 2018-11-04 12:00:00
term: 1
published: true
---

<span class="text-small">*Introduced by Jose Luis De Vicente, Pau Asina and Indy Johar*</span>

![]({{site.baseurl}}/TroikaSpeculativePlant.jpg)
*Troika, plant fiction: Selfeater, 2010*


### An age of uncertainty

TIME SCALE
IS IT THE FUTURE?

We obsess over the future and pray tomorrow will be better than yesterday, even though we only have the now. We are programmed to seek out the new, a sort of neophilia that makes us want to find the inexperienced and the undiscovered. In this accelerated pace of change, can human beings be resilient to this these new contexts?

**What is the impact of anthropocene?**
Have we entered a new geological age in which human beings are not part of a geological age of the Earth anymore?
How to measure temporal scale when the sense of time and reality can not be fully defined? What is the present? What is the resolution at which we are operating?

We are in an age of uncertainty, in which a number of factors are involved and playing an important role in what future we are defining for ourselves.

How can we define *future*?
Future as now? Future as non-existent but only the present as a form of future.

Believing the future is now, would mean you live in your philosophy, you don’t get overwhelm with the weight of the many futures we want to create for ourselves. What is design for the future? It is design for the now, a direct action is taken. The present could also be seen as a form of future too.


### Humans of a world in alert

ENVIRONMENTAL CHANGE
UTOPIA AND DYSTOPIA

Climate change isn’t just a *technical problem* but a political, economical and cultural problem too.

In the past month, months, years, decades, century humans have triggered and experienced a series of events that have had a great impact on the evolution of economies, politics, down to the individuals’ behaviours and has greatly changed the environmental status.
Can we link recent events, such as Catalonia referendum, Brexit, Trump’s election, stock market crash and 7/11 terror attack, with the highest historical level of CO2 today?

I started reading Naomi Klein’s book “*No Is Not Enough*” in which she is depicting large-scale crisis or shocks to societies as a result of political shifts. She defines the term “shock doctrine” as “the tactic of systematically using the public’s disorientation following a collective shock - wars, coups, terrorist attacks, market crashes, or natural disasters - to push through radical pro-corporate measures often called *shock therapy*.” (N. Klein, *No Is Not Enough*, p3)

This deep confusion, now a global phenomenon, is affecting our interests, values, and actions (or stillness). We no longer care because we do not know anymore how to relate events and our ability to understand and explain those. That is one of the ways we have come to deny climate change or invented time we do not have to solve the growing issue.

Naomi Klein also wrote about ExxonMobil and how this company knew about the climate change back in the seventies and did nothing despite the warnings from their senior scientist James Black, who back then talked about the fact that there’s this “general scientific agreement that the most likely manner in which mankind is influencing the global climate is through carbon dioxide release from the burning of fossil fuels” (Klein, N.(2017)*No is Not Enough*, p.67)

Since Trump’s election, a lot of infromation concerning climate change has been kept for the bin. “There are plans to cut NASA program that uses satellites to accumulate basic data on how the Earth is changing” (N. Klein, *No Is Not Enough*, p75).
Let’s share what NASA has observed while they can.

![]({{site.baseurl}}/NASAco2Graph.jpg)
[NASA Global Climate Change](https://climate.nasa.gov/climate_resources/24/graphic-the-relentless-rise-of-carbon-dioxide/)

This graph conveys scientific measurements that can answer to this question: the humans have a great capacity to change the climate and planet and these past events and future events have and probably will increase the risk of dramatically transforming the ecosystem.

![]({{site.baseurl}}/NOAA Research CO2 Trends Monthly.jpg)
[NOAA Research](https://www.esrl.noaa.gov/gmd/ccgg/trends/index.html#mlo_growth )

For the past 1 million years, the atmospheric CO2 has ranged between 172 and 300 part per million (ppm).
Before the Industrial Revolution started in the mid-1700s, the global average amount of carbon dioxide was about 280 ppm.
By the time continuous observations began at Mauna Loa Volcanic Observatory in 1958, global atmospheric carbon dioxide was already 315 ppm. On May 9, 2013, the daily average carbon dioxide measured at Mauna Loa surpassed 400 ppm for the first time on record. Less than two years later, in 2015, the global amount went over 400 ppm for the first time. ([R.Lindsey, *Climate Change: Atmospheric Carbon Dioxide*](https://www.climate.gov/news-features/understanding-climate/climate-change-atmospheric-carbon-dioxide))

During this *big acceleration* in the last 50 years, many things have radically changed, such as fertiliser consumption, co2 production, floods and other natural catastrophes, and we have now a population on this planet that has tripled in size.

**We have reached a point of no return and this is no news.**

Climate change is still a major concern, happening now, and yet this information about change is not making the front page news. Why? We don’t know how to deal with problems that make us feel powerless. How can we adjust psychologically from the state of trauma to the empathy? And one of the consequences of this trauma is developing fear towards the potential of new technologies.

**So what do we do?** and **how do we move towards a clean economy focused on renewables?**
Utopia for sustainability vs. Apocalyptic Dystopia

In 2015, the conference in Paris was organised to discuss an agreement between countries to tackle the problem of climate change by accepting to reduce a defined % of CO2 in a specific period of time. This target for 2030 is aimed to be achieved by cutting at least 40% of CO2 production.

Meanwhile, methods to capture CO2 from atmosphere have been developed, starting with Lackner’s carbon removing device, potentially using more energy to remove it than to produce it.
What would this mean? We need an alternative method to reduce or remove and eventually produce, using less energy. In 2009, the Canadian firm Carbon Engineering say they have taken a big step forward on cutting the costs of direct air extraction. With funding from Microsoft's Bill Gates and Canada oil sands financier Norman Murray Edwards, their pilot plant has been running since 2015, capturing about one tonne of CO2 per day. ([M.McGrath, Key 'step forward' in cutting cost of removing CO2 from air](https://www.bbc.com/news/science-environment-44396781))

1973, Apollo mission - it was the first and last picture ever taken of the planet earth by a human. Since that day, this image of the planet Earth has been perpetuated until today. Has it become a myth? Is the Earth still looking like this? There are many things that are changing that we don’t know about. Many aspects of climate change are not traceable, however we can understand the current economic, cultural, social and environmental status by looking at our landscape.

![]({{site.baseurl}}/TomHegen.jpg)
Photographs by Tom Hegen

In the book [*Speculative Everything*](http://readings.design/PDF/speculative-everything.pdf), Dunne and Raby explore the power of critical design through the perspective of speculative design in today’s context.

It suggests this approach is used “to challenge narrow assumptions, preconceptions, and givens about the role products play in everyday life.” It highlights weaknesses of a design, system or preconceived ideas within the existing reality, allowing us to reimagine our reality and our relation to it.

Critical thinking is achieved through a series of *what if* questions  translated as tangible design proposals. Those aim to suggest behaviours, interactions and uses within that alternative reality. Ultimately, it allows the individuals to focus more on “our defined laws, ethics, political systems, social beliefs, values, fear and hopes”.

**“ You can ask an audience to believe the impossible but not the improbable. “**

Speculation is “a tool” used to create “not only things but ideas.” “The ideas freed by speculative design increase the odds of achieving desirable futures”. It can act as “a catalyst for collectively redefining our relationship to reality”.

At the time, when I read this book I understood this was a direction I could take and I believed it could, not only, give power to anyone but also could let you dream and create new ones within our conflicting world. I now feel it is somehow too easy to keep dreaming and suggesting things in a way that will always limit its development or application. Those speculations form as messages but also as products. It hard to imagine a product - as it is defined in society - that could keep adapting, changing, but instead it seems easier to see it become an obsolete object.
What happens the day the dream you’ve created with speculation is not as relevant anymore? Would another “speculative product” be created and so on.

Would it be more sustainable and resilient if that product was an ever changing project, a system, a method to put in place in our current reality to make it more accessible, plausible and therefore subject to change?

I feel what is missing is the agent, using the same aims as critical design but a different methodology. Through interventions, we can test and experiment in context, to see results and reaction, helping to adapt and continue designing this idea with the end goal to insert in real time within our society.


### A society resistant to change

RESPONSABILITY
OCULARCENTRIC CULTURE
SOCIAL CONSTRUCT

The ideal action would be to break the equation of growth formed by the capitalist system by changing basic parameters of the capitalist system.

Would the promotion of connection, interaction and responsibility towards the natural environment (other bio-organism), achieved with the development of a symbiotic system, be a solution?

How to implement **a radius of responsibility** within those current parameters? How to use design as a political tool?

Reading and analysis of *“Unfolding the political capacities of design”*, a book by Fernando Dominguez Rubio and Uriel Fogué.
This paper is being used as a tool to understand what and how emerging ideas and methodologies are born. Exploring the different links and positions between design and society.

Terms used during the reflective conversation:
*Epistemology* (the studies the scope of knowledge)
*Ontology* (branch of metaphysics looking at the nature of being)
*Modernity* (tendency to separate things and label them)
*Ergonomics* (a physical language between psychology and design)
*Body politic* (a collective body of people belonging to a country or society)
*Disciplinary society* (power exercised through disciplinary means)

**What is society made of?**

The social aspect tends to be the first thing that comes to mind when defining society. The social as in the relationship between people and how it is constantly evolving.
If it is social, then what is the role of design in society?

**What are the political capacities of design?**

*”Design constructs society”*
How does design construct, or what are the things constructing, our relationships between people?

*“Designers produce society”*
Design as producer of relationship, therefore producer of society.
For instance a floor could be constructing a relationship.
We have an extreme materialistic approach to society, meaning the behaviours around those objects and materials are building relationships (those change depending on the cultures).

Michel Foucault (1975) studied how power can be spread through material things in France during the 17th Century.
The *“chairs silently brought the body into the territory of power by setting the physical parameters of what is a ‘correct’ bodily position…” p.2*

![]({{site.baseurl}}/BehaviourChairStudy.jpg)

“Body politic” and “disciplinary power” are terms that have been used frequently by Foucault. The body politic is subjected to that power instead of seeing the “architecture of power”.
Education seen as a tool, constructing the education of citizens.
A level of consciousness is added, a *moral demand* such as to slow down.

Design as a way to develop an urban form, an infrastructure? A sort of structure defining our behaviour in relation to other elements as part of society or the environment.
Design is used to define relationships between things.

The *”performativity of design”* seduces you to do things in a certain way but does not force you. It is a choice made by the individual introduced to that design.
Design is creating worlds and identities. Is it then an intellectual space or a behavioural space that is constructed?

How do we make a change in our everyday lives to help stop climate change?

Does a thing exist by itself if there is no viewer? *”In The Eyes of the Skin”* is a book by Juhani Pallasmaa, depicting how technology has allowed us to create more and new sensorial experiences, responding to the ocularcentric culture humans have built over time. Vision makes us believe that it can give us immediate access to the reality we live in, meaning it would give some sort of proximity. I believe this phenomenon is mainly increasing the imbalance in our sensory system, keeping us away from truly sensing and understanding our built environment. But most importantly it is changing the way we see and value things.

“Scientific facts are a social construct”

Language is a social construct, it is based on

Scientific fact were affirmed by measurements and observations. This perception and our tools to measure have defined what is considered *fact* for society. From there we create a consensus based on those continually refined hypothesis to decide what is the fact. Through those accepted truths and norms, a consensus is achieved about precedent hypothesis and results. Once this is accepted by society, it is deeply anchored in our beliefs of what is reality.


### Micro-interventions

DESIGN IS A POLITICAL TOOL
BODY AS AN INTERFACE
READ-WRITE TECHNOLOGIES

One method to solve the problem and remove any kind of fear and distance would be by dismantling it.

![]({{site.baseurl}}/DismantleSketch.jpg)

Make the process of radicalisation as fast as possible can have significant change. Once the bigger problem has been dismantled into different aspects of the issue, we can analyse the political, social and economical problems.

**Micro-actions are political**

Those actions are found in the everyday practices, it is our philosophy as a designer and as an individual in this society.
*“blurring distinction between public and private spaces or between political actions and everyday practices” p.8*

**If the direct impact of actions was quantified or measured, what would change?**

The success of this idea depends on the whole material network, system behind it. In order to design a product, a whole system needs to be put in place for it to work.

Is society technologically configured?
Used the term *”black box”* as of means to hide how things are developed. What happens if it became transparent, open source for anyone to see?

[Jane Fonda Kit House](https://www.domusweb.it/en/architecture/2012/08/21/elii-jf-kit-house.html) designed by Elii Studio *“aims to be a “black box” in the theatrical sense of the term”. p.16*
“To stage problems, to render them evident and public” p.18* which enables to see sustainability as a political problem through technology and architecture.
This house designs entities and questions.

**We know our body through what our body does.** In this sense the context contribute strongly to the meaning. In the same line of thought, we know our environment through what our body senses. In a way, our existence is based on a bottom-up approach.

**Is everything technology?**
What is technology?
Technology is the old one and the new one, such as internet and
We use technology in matter to construct things.

What is not technology?
Is there a particular type of society where no technology would be involved? Is an action as a technology? Or is the human as the technology?


### Read-write technologies

The process of exchange of information and symbols.

Trees are endowed with a true form of intelligence, allowing forms of communication. They occupy nearly a third of our territory and the world's land surface. A German forester called Peter Wohlleben explores in his book, *La Vie Secrète des Arbres* the idea that trees communicate using like a nervous system to connect.

In a way all physical systems are based on this exchange of information.

*”Life 3.0: Being Human in the Age of Artificial Intelligence”* by Max Tegmark (https://www.youtube.com/watch?v=ImrBfVK10AY go to 18:08)

Max Tegmark defines life 2.0 with a metaphor with a human as a computer, able to upload new softwares into its mind and gain a new set of skills. Life 3.0 doesn’t exist yet but can be created with the development of AI, for the better or for the worst.

What we consider as interfaces are changing  the course of civilisation, and the human body is one of them.
Our senses are acting as the interface. For instance, the eyes see the visible light spectrum.
If the body was given a device as a new interface adding up to the total skills available for the body to use. What would happen if the body was given new skills such as seeing x-ray?
[*”The Secret History of X-ray Specs”*](http://www.bbc.com/future/story/20160718-the-secret-history-of-x-ray-specs)

What if we were being able to see through walls into the most intimate spaces and people’s most private actions. What would our cultural ideas of privacy, limits and boundaries become if we saw things in a different way? What would the idea of *trust* be? And how would we define the idea of *transparency*?

What is the ultimate interface of the 21st Century?
Body? Time? Camera?
All three are, combined to form one interface leading to the practice of photography.
A camera has the power to capture a short moment and make it permanent. It is can reveal things that might not be seen by all at all times. This combination of three has created a past, present and future. We have been able to look into the past through photography and are now creating our future by sharing projections of ideals instantly. These interface have created a language that can redefine what we understand as time in relation to the body, the camera and our environment.
Lenses have now a major influence in our everyday lives, by defining our identities, our values and our contexts. We trust in what the eyes see through the lense, but what if the lense was a hoax?

Who or what is validating the definition of one’s identity and creating the norms of our society?


### Vision development

PARAMETRIC DESIGN


**Quis sum**

Earlier this week we discussed how important it is to keep doing things for pleasure and not feel the need to only do things that actively relate to our beliefs as a designer.
A few weeks ago, I realised that I was constantly keeping myself from doing things I enjoyed because I feel they are too selfish. I thinkt to be selfish, in a time of global crisis, is not appropriate because there’s more important things that should be done with the short amount of time we have on this planet to try make a change for the better.
But then, isn’t it ironic to think I can help society and our environment if I don’t even know who I am and what I want to be as part of this society?

What I enjoy to do in my free time could be helping me understand who I am. Here’s a some of my reflections based on the things I like or know about myself.

Climbing is a sport I have recently rediscovered, and I realise it has now become something I really take pleasure in doing and want to continue. Why? Challenge is something I am constantly looking for, and I know I sometimes take too challenges that I can not take by myself but in climbing, those challenges are kept manageable. Choice is a luxury I have had in many situations but I do struggle to make decisions. With the many routes there is, I know that it doesn't matter which one I take because I can always fail and try something else. Each of these have different characteristic which help me make my decisions. I also know that there’s not one way of doing it and I can rapidly try different methods to achieve my goal. Some need technicality, others need more endurance, and all together they help me acquire different skills and train them in different ways. This sport requires a body awareness that I discovered as a dancer and enjoy a lot. In order to reach that  it needs a lot of observation of the environment and the people. When I go climb out door, it is a whole new sensorial experience, highly aware of the environment and using the body to communicate with it. It is sort of a choreography on the wall which is only possible to do with a cliff or rock.
Frustration is something I can’t escape from in this but it doesn’t last and is not something I will keep in my mind constantly. For a reason or another, I manage to think only of climbing when I’m there and to stop thinking of it when I finish.

All this to say I would like to try look at it as a metaphor and apply some of those aspects in my design philosophy.

Photography is another thing I like to do in my leisure time, but it doesn’t take as much time, therefore I don’t feel like it is a sacrifice and I already use it in my work. However I could push this passion and experiment more with the methods and results. It is an art helping me to share a perspective. Film camera takes a bit of money and more time but it is convenient for experimentation. I use it as a way to capture moment but also remember the relation with environment at that precise moment.


**Empathy as a strength or weakness?**

I am a very empathetic and sensitive person which means I value connection over control. I take on easily the emotions of others on top of my emotions, which makes me understand the reality through the perspective of others instead of helping me develop a perspective my own.

This is one of the factors that could explain the struggle to define who I am and who I want to be. This brings me to ask the following questions as a reflection on ways to use empathy as a strength. How to develop my own personality and strengthen who I want to be and what I want to do, when I am taking from connections and others perspectives? How can I cultivate this empathy and sensitivity as a strength instead of trying to push it away? Could I help promote an empathic culture within a working environment? How can I use the empathy to help others understand difference and conflict from diverse perspectives to promote understanding and cohesion? Could I use it to help create a safe place for exploration, without being scared of mistake, for myself and others?


**Network**

What are the element of the network that will form a symbiotic system?
Create a network I will need to have this multidisciplinary approach for collaboration
Make improbable links? Test new connections between things.
Network as a distribution of information and the grouping of information.
A "breathing" network that can contract and expand depending on the situations.
I aim to construct problem-solving methods as parametric design, which would allow ideas to constantly reshape and reconnect in new ways.


**Interests to initiate a project**

Why do I want to create a symbiotic system?

- Merge two speeds that are coexisting to slow down the rhythm.
    Could I start playing with different methods using my interest and knowledge in music?

- Two systems (linear and circular) as one. Is it about making compromises?

- Towards sustainability

- Connecting the physical and the digital worlds by re-enforcing the bridge between our natural environment and the reality we’ve created.
    Could I use the climbing metaphor or my interest in food, cooking and gardening to   find a way to achieve this?

We talk about post machine era where information will be everywhere and available at all times
What if that information is not accessed through an AI but through organic, living things from the natural environment? Living organisms as an interface?

Create a common language between nature, humans and machines.
Communication method - create interactive visuals that will construct itself?

How do I make it happen in two weeks?
What piece of the system to work in small scale? A community? A government? An industry?
How to keep a small emergent community producing new things? Provoke.
Does a community have to be physical?
Kickstarter project?
Body as an interface?

What do I aim to find?


---------------------


### Reading list

*“No Is Not Enough”* by Naomi Klein

*”How The World Works” by Noam Chomsky

*”Massive Change”* by Bruce Mau

[Paris Decleration 2005 - Acra Agenda Action 2008](http://www.oecd.org/dac/effectiveness/parisdeclarationandaccraagendaforaction.htm)

[Culture of Resilience](http://culturesofresilience.org/wp-content/uploads/resources/CoR-Booklet-forHomePrinting.pdf) (Ideas)

[*Escape the Smart city*](https://repository.tudelft.nl/islandora/object/uuid%3A21347cc6-d692-4560-9341-46e477a9a653?collection=education&fbclid=IwAR0I-J3D5lPrO8PMknP_xGMM-EsYxjcCrmXFgY7HR68CY4dNcdMPmxsb5SQ
)
“Escape the Smart City is a critical pervasive game for creating awareness about the implications of AI-surveillance technology in the smart city. It responds to growing concerns over the mass deployment of surveillance cameras that are enhanced with artificial intelligence (AI) which are turning the cities into digital panopticons (Sadowski & Pasquale, 2015)”
A “ *black-box* which inhibits public from understanding it and having a say in its deployment.”

Yuval Noah Harari: *The Myth of Freedom*
https://www.theguardian.com/books/2018/sep/14/yuval-noah-harari-the-new-threat-to-liberal-democracy

Blockchain
https://medium.com/humanizing-the-singularity/why-global-trade-will-inevitably-move-to-the-blockchain-ab3f66bd20f8
https://www.coindesk.com/blockchains-killer-app-making-trade-wars-obsolete/
https://hbr.org/2017/03/what-blockchain-means-for-the-sharing-economy
https://provocations.darkmatterlabs.org/the-societal-contract-for-innovation-15593ae9a1d4

[CERN](https://home.cern/about/what-we-do/our-impact)

[Jeremy England theory on evolution being distributed by energy exchange](http://nautil.us/issue/50/emergence/how-do-you-say-life-in-physics-rp)

[SUPERFLUX - *Experience goes where data can’t*](http://superflux.in/index.php/work/how-will-we-live/#) article by Anab Jain

[*All Reality is Interaction*](https://onbeing.org/programs/carlo-rovelli-all-reality-is-interaction-mar2017/) by Carlo Rovelli

[*Why trees don’t ungrow*](https://aeon.co/essays/does-the-flow-of-heat-help-us-understand-the-origin-of-life)

[*A New Physics Theory of Life*](https://www.quantamagazine.org/a-new-thermodynamics-theory-of-the-origin-of-life-20140122/)
Jeremy England on his idea that “life exists because the law of increasing entropy drives matter to acquire lifelike physical properties”.

[*The Next Revolution in Photography Is Coming*](http://time.com/4003527/future-of-photography/?xid=fbshare) article by Stephen mayes (2015)

http://bigbangdata.cccb.org/en/topic-thinking-doing-feeling/
http://www.janavirgin.com/
http://platoniq.net/en/
http://bigbangdata.cccb.org/en/

*"Leverage Points: Places to Intervene in a System"* by Dr. Donella Meadows
[*”Thinking in Systems”*](tem.com/uploads/Donella_H_Meadows__Diana_Wright.pdf) by Donella H. Meadows.

http://www.donellameadows.org/wp-content/userfiles/Limits-to-Growth-digital-scan-version.pdf

[Suzanne Simmard on information exchange networks in forest sub-soils](http://www.radiolab.org/story/from-tree-to-shining-tree/)

[The difficulty of defining boundaries in biology](https://aeon.co/essays/)

[What constitutes an individual organism in biology](https://www.popmatters.com/we-have-never-been-human-2495438275.html)

George Orwell, *‘Nineteen Eighty-Four’*(Book 1949)

Dave Eggers, *‘The Circle’*(Novel 2013, Film 2017)
